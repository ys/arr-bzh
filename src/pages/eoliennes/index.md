---
templateKey: eoliennes-page
path: /eoliennes
title: NON AUX CENTRALES ÉOLIENNES DANS LES MONTS D'ARRÉE
image: /img/eoliennes.png
heading: Protégeons les derniers refuges de la faune et de la flore !
testimonials:
  - title: Témoignages d'habitants qui subissent le voisinage des éoliennes industrielles dans l'Allier.
    video: https://www.youtube-nocookie.com/embed/Sa3dRnWBRps
    quote: >-
        Les gens y allaient de bon cœur pour toucher le pactole, le super jackpot... Mais le jackpot maintenant, c'est les conséquences.
    author: Jean-Dominique Barraud, maire de Lavoine
  - title: "L'impact des éoliennes sur le bétail"
    video: https://www.youtube-nocookie.com/embed/Ur3-nTlo4AE
    quote: >-
        en 2011, baisse de presque 2000 litres de production de lait par vache. [...] En 2015, je n'ai pas eu d'autre solution que de licencier mon salarié et d'arrêter l'activité laitière.
    author: Yann Joly, éleveur
  - title: "Un référendum local dit non aux éoliennes"
    video: https://www.youtube-nocookie.com/embed/TVIIPNszyj0
    quote: >-
        Je pense que les deux avis sont présents dans le conseil municipal actuellement, mais le gros problème c'est que, pour certaines personnes, c'est le versant financier qui a pris le dessus sur le versant écologique.
    author: Thomas Hennequin, conseiller municipal à Lislet (02)
---

- Notre santé en danger
- Perte de valeur de notre patrimoine et d’attractivité de notre territoire
- Paysages défigurés
- Dévalorisation des biens immobiliers
- Perte de biodiversité et fuite du gibier
- Augmentation de nos factures d’énergie sans diminution des émissions de CO2
- Finies les nuits étoilées !

Les Monts d’Arrée sont actuellement la proie de promoteurs éoliens, et la commune de Berrien est une de leurs premières cibles.

La Mairie a initié un projet éolien sur la commune de Berrien, sans consultation de ses habitants. Une association de « citoyens-actionnaires » a même été créée à l’initiative de la Mairie, afin d’entrer dans une « société de projet » tripartite (promoteur éolien, commune et cette association). Il s’agit en réalité d’un simulacre de démocratie.

Les éoliennes seraient implantées entre le Bourg, Quinoualc’h, Cozcastel, Keraden, Kernevez et Tredudon-le-Moine. Un tel projet paraît insensé aux yeux des habitants qui aiment les Monts d’Arrée pour la beauté de ses paysages et pour la qualité de vie qu’on peut y trouver.

**Seul un NON ferme de la population pourra empêcher ce projet !**

## Des profits privés aux dépens du climat !

Plusieurs milliards d’euros par an sont prélevés sur nos factures d’électricité (CSPE) et de carburants pour subventionner des promoteurs éoliens privés. En France, le déploiement de l’éolien ne permet pas de réduire les émissions de gaz à effet de serre ; au contraire il est souvent associé aux centrales à gaz puisqu’il faut de toutes façons une source d’électricité rapidement pilotable quand il n’y a pas de vent ! Cet argent donné aux promoteurs pourrait servir à financer des mesures d’économie d’énergie (isolation des bâtiments…), les énergies renouvelables thermiques (pompes à chaleur…), les transports en commun, etc. bien plus efficaces pour le climat et pour pouvoir enfin fermer des centrales !
En plus, l’éolien industriel cause d’autres problèmes écologiques : pales non recyclables, des kilomètres de lignes électriques à tirer et des transformateurs électriques à ajouter, consommation de grandes quantités de matériaux…

## Des paysages ruinés !

Le Parc Naturel Régional d’Armorique, les Monts d’Arrée, le Pays COB : ils regorgent d’espaces naturels remarquables d’une exceptionnelle diversité, qui sont au cœur de l’attractivité du territoire. Ce patrimoine naturel unique apparaît comme un élément-clé de l’identité régionale et contribue largement à la qualité de la vie locale et au tourisme. Il doit être préservé des projets industriels qui portent atteinte aux humains, à la biodiversité, aux paysages et aux écosystèmes des Monts d’Arrée.
D’autres projets seraient en cours à La Feuillée, Brennilis… L’expérience montre que là où des éoliennes sont implantées une première fois, d’autres suivent jusqu’à saturation des paysages !
De plus, là où il y a des éoliennes, les maisons et gîtes ruraux perdent entre 20 et 45% de leur valeur et l’économie locale peut être impactée négativement, notamment le tourisme.

## La biodiversité massacrée !
Les éoliennes industrielles créent un vide écologique autour d’elles : fuite du gibier, hécatombe de chauve-souris et d’oiseaux de proie protégés, destruction de leurs habitats, artificialisation des sols…
De nuit, les lumières aveuglantes des éoliennes perturbent également les oiseaux migrateurs et stressent les animaux qui ne peuvent plus se reposer sereinement.

## Notre santé et les élevages en danger !

L’État ne mène aucune étude approfondie sur les impacts des éoliennes sur la santé des humains et des animaux. Or certains éleveurs subissent des pertes importantes, en qualité et quantité pour le lait, et une mortalité anormale dans leurs troupeaux, par exemple à Nozay (44). Des études ont aussi montré des taux de cortisol (marqueur du stress) plus élevés dans des élevages de porcs proches d’éoliennes.
Dans beaucoup de pays d’Europe la distance minimale aux habitations est bien plus élevée qu’en France où elle est seulement de 500 mètres pour des éoliennes qui peuvent désormais atteindre plus de 200 mètres de haut !
L’impact sonore sur les riverains est évalué à 35 dBA (déjà désagréable, le bruit des éoliennes étant quasi permanent) mais est en fait bien plus important pour certaines fréquences d’infrasons et il peut y avoir des résonances aux effets
multiples (véranda qui vibre, échos dus à la topographie des lieux…). Les personnes vivant à proximité sont dérangées par les flashs lumineux rouges la nuit et par des effets stroboscopiques le jour…

## L’arnaque des baux emphytéotiques…
Les industriels éoliens préfèrent toujours louer les terres, alors que cela leur coûterait beaucoup moins cher de les acheter. Pourquoi ?
Lorsqu’une éolienne est obsolète et que l’exploitant (souvent une société-écran) dépose le bilan c’est le propriétaire qui est responsable de ce qui est sur son terrain. Dans ce cas, il ne peut compter que sur les 50 000 euros qui sont provisionnés pour le démantèlement d’une éolienne, alors qu’en réalité cela coûte plus de 150 000 euros ! Et en dernier recours c’est le contribuable qui paiera, donc nous tous !
Et même si l’exploitant effectue le démantèlement, il n’a aucune obligation
d’enlever les fondations en béton armé (jusqu’à 2 000 tonnes par pied d’éolienne), qui, en se dégradant, pollueront l’environnement !
