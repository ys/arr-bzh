---
templateKey: index-page
title: ARRÉE RÉSISTANCE RURALE
image: /img/Monts-d-Arree.jpeg
heading: NON aux centrales éoliennes dans les Monts d'Arrée
subheading: NON aux centrales éoliennes dans les Monts d'Arrée
mainpitch:
  title: >-
    Les Monts d'Arrée sont actuellement la proie de promoteurs éoliens, et la
    commune de Berrien est une de leurs premières cibles.
  description: >-
    Seul un NON ferme de la population pourra empêcher ce projet !
testimonials:
  - title: Témoignages d'habitants qui subissent le voisinage des éoliennes industrielles dans l'Allier.
    author: Jean-Dominique Barraud, maire de Lavoine
    quote: >-
        Les gens y allaient de bon cœur pour toucher le pactole, le super jackpot... Mais le jackpot maintenant, c'est les conséquences.
    video: https://www.youtube-nocookie.com/embed/Sa3dRnWBRps
---
### Protégeons les derniers refuges de la faune et de la flore !

La **Mairie de Berrien** a initié un projet éolien sur la commune, sans consultation de ses habitants. Une association de « citoyens-actionnaires » a même été créée à l’initiative de la Mairie, afin d’entrer dans une « société de projet » tripartite (promoteur éolien, commune et cette association).
Il s’agit en réalité d’un simulacre de démocratie.

**Les éoliennes** seraient implantées entre **le Bourg**, **Quinoualc’h**, **Cozcastel**, **Keraden**, **Kernevez** et **Tredudon-le-Moine**. Un tel projet paraît insensé aux yeux des habitants qui aiment les Monts d’Arrée pour la beauté de ses paysages et pour la qualité de vie qu’on peut y trouver.

- Notre santé en danger
- Perte de valeur de notre patrimoine et d’attractivité de notre territoire
- Paysages défigurés
- Dévalorisation des biens immobiliers
- Perte de biodiversité et fuite du gibier
- Augmentation de nos factures d’énergie sans diminution des émissions de CO2
- Finies les nuits étoilées !

**Nous demandons L’ABANDON DU PROJET DE PARC ÉOLIEN À BERRIEN** et nous nous opposons à tout projet éolien industriel dans les Monts d’Arrée.
